#!/usr/bin/env python

import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.storagelevel import StorageLevel
from pyspark import SparkFiles


#conf = (SparkConf().setMaster("yarn-client").setAppName("point-in-polygon").set("spark.executor.memory", "2g").set("spark.executor.instances", 16))
#sc = SparkContext(conf = conf)
sc = SparkContext(appName="TAZ based Taxi density")
sc.addFile("shapefile.py")
sc.addFile("quadTree.txt")
sc.addFile("shape/NY_TAZ.shp")
sc.addFile("shape/NY_TAZ.dbf")
sc.addFile("shape/NY_TAZ.shx")

import shapefile
mShp = SparkFiles.get("NY_TAZ.shp")
mTree = SparkFiles.get("quadTree.txt")
e = shapefile.Editor(str(mShp))
e.buildQuadTree(str(mTree))

input = sc.textFile('hdfs://cg-hm08.ncsa.illinois.edu/user/jyn/ny_taxi_1.csv')

def getindex(line):
    point_data = record.strip().split(",")
    if len(point_data) < 3:
        return []
    if point_data[5] == "":
        return []
    if point_data[11] == "":
        return []
    lat = float(point_data[11])
    if point_data[10] == "":
        return []
    lon = float(point_data[10])

    if point_data[7] == "":
        return []
    passenger_count = int(point_data[7])

    ind = e.index_of_first_feature_contains_point(lon,lat)

    if ind != -1:
        return [(ind,(passenger_count, 1))] 
    else:
        return [(ind,(0,0))]

output = input.map(getindex).reduceByKey(lambda a,b: (a[0]+b[0],a[1]+b[1]))
#.map(lambda (key,value):(key,(value[0]*1.0)/(value[1]*1.0))).sortByKey()

output.saveAsTextFile('hdfs://cg-hm08.ncsa.illinois.edu/user/jyn/assign2_result.txt')

