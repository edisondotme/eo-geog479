import sys
import time
from pyspark import SparkContext
import math
#import numpy as np

def makeIndex(record):
    '''filter and indexing target point records'''
    # Invalid Record Filter
    point_data = record.strip().split(",")
    if len(point_data) < 1:
        return []

    x = float(point_data[0])
    if x < start_x or x > end_x:
        return []

    y = float(point_data[1])
    if y < start_y or y > end_y:
        return []

    row_index = math.ceil((y - start_y) / grid_size)
    col_index = math.ceil((x - start_x) / grid_size)

    #return [(format %(row_index, col_index),(radiation_value,1))]
    
    return [((row_index, col_index),(1, 1))]


if __name__ == "__main__":
    sc = SparkContext(appName="Chicago Density")
    dataset = sc.textFile('hdfs://cg-hm08.ncsa.illinois.edu/user/jyn/chicago_2014_traj_rank_moments.txt').filter(lambda x: True).cache()
    #paras = sys.argv[-5:]
    
    #start_x = float(paras[0])
    #end_x = float(paras[1])
    #start_y = float(paras[2])
    #end_y = float(paras[3])
    #grid_size = float(paras[4])

    start_x = -30.0
    end_x = 30.0
    start_y = -30.0
    end_y = 30.0
    grid_size = 0.2
    
    #ncols = math.ceil((end_lat - start_lat) / grid_width)
    #nrows = math.ceil((end_lon - start_lon) / grid_height)

    result = dataset.flatMap(makeIndex).reduceByKey(lambda a,b: (a[0]+b[0],a[1]+b[1])).map(lambda (key,value):(key,value[1]*1.0)).sortByKey()
    #ww = result.collect()
    #print max([l[0][0] for l in ww]), max([k[0][1] for k in ww])
    result.saveAsTextFile('hdfs://cg-hm08.ncsa.illinois.edu/user/jyn/chicago_density_test.txt')
    #print '[Doine]!!!!!'
