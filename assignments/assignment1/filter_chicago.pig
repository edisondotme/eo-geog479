REGISTER piggybank.jar;
DEFINE Get_Time org.apache.pig.piggybank.evaluation.datetime.convert.CustomFormatToISO();
DEFINE To_ISO org.apache.pig.piggybank.evaluation.datetime.convert.ISOToUnix();
%declare IN_DIR '';
%declare OUT_DIR ''

-- Load the Data
raw = LOAD '${IN_DIR}${input}' USING PigStorage('\u0001') AS (tweetId,text : chararray ,geo,source,isretweet,search_id,create_at,meta_data,to_user_id,language_code, user_id,profile_image_url,user_name,place_id,place_name,place_fullname,country,place_type,street_address,boundary,boundary_type,place_url);

-- Filter out duplicates
clean = DISTINCT raw;

--Filter out missing data
step0 = FILTER clean BY geo is not null AND geo != 'null' AND  geo != '' AND create_at is not null AND create_at != 'null' AND create_at != '';

-- Separate location (into latitude and longitude) and format time.   $0:user_id $1:long, lat $2 $3: time $4:text
step1 = FOREACH step0 GENERATE user_id, flatten(STRSPLIT(geo, ',')), To_ISO(Get_Time(REPLACE(REPLACE(create_at, 'CDT', '-05:00'), 'CST', '-06:00'), 'EEE MMM dd HH:mm:ss Z yyyy')) , text;

--The above step changes timestamp to Unix format, alternatively, it is possible to have readable time strings
--step1 = FOREACH step0 GENERATE user_id, flatten(STRSPLIT(geo, ',')), create_at, text;

-- Select the points in US boundary (in this case, bounding box)
-- $2 is longitude, $1 is latitude
step2 = FILTER step1 BY $2 > -88.707599 AND $2 < -87.524535 AND $1 > 41.201577 AND $1 < 42.495775;

-- Generate output
--step3 = FOREACH step2 GENERATE $0, $1, $2, $3;
--STORE step3 INTO '${OUT_DIR}${input}_filtered.txt' USING PigStorage(',');

-- Filter keywords and generate the output for scenario table
step4 = FILTER step2 BY $4 MATCHES '.*(happy|sad|Chicago|traffic|weather).*';
step5 = FOREACH step4 GENERATE $4;
STORE step5 INTO 'chicago_tweets.txt' USING PigStorage(',');
