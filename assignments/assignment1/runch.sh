# Delete the data from previous run

#hdfs dfs -rm 
clear
rm chicago_tweets.txt
rm chicago_sorted_tweets.txt
hdfs dfs -rm -r chicago_tweets.txt 
hdfs dfs -rm -r 2014_03_01_tweets.txt_filtered.txt


# Re run hadoop job

pig -f filter_chicago.pig -param input=2014_03_01_tweets.txt

echo "Almost done... sorting"

hadoop jar /usr/hdp/2.3.2.0-2602/hadoop-mapreduce/hadoop-streaming-2.7.1.2.3.2.0-2602.jar -file mapper.py -mapper mapper.py -file reducer.py -reducer reducer.py -input chicago_tweets.txt -output chicago_sorted_tweets.txt
# Download the data from hdfs

hdfs dfs -getmerge chicago_sorted_tweets.txt

# sort those tweets

sort -t',' -k2 chicago_sorted_tweets.txt

echo "Completed"

