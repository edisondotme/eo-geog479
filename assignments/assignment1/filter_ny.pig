REGISTER piggybank.jar;
DEFINE Get_Time org.apache.pig.piggybank.evaluation.datetime.convert.CustomFormatToISO();
DEFINE To_ISO org.apache.pig.piggybank.evaluation.datetime.convert.ISOToUnix();
%declare IN_DIR '';
%declare OUT_DIR '';

-- Load the Data
raw = LOAD '${IN_DIR}${input}' USING PigStorage(',') AS (medallion, hack_license, vendor_id, rate_code, store_and_fwd_flag, pickup_datetime:chararray, dropoff_datetime, passenger_count, trip_time_in_secs, trip_distance, pickup_longitude, pickup_latitude, dropoff_longitude, dropoff_latitude, payment_type, fare_amount, surcharge, mta_tax, tip_amount, tolls_amount, total_amount);

-- Filter out duplicates
clean = DISTINCT raw;

--Filter out missing data
step0 = FILTER clean BY pickup_latitude is not null AND pickup_latitude != 'null' AND  pickup_latitude != '' AND pickup_datetime is not null AND pickup_datetime != 'null' AND pickup_datetime != '';

-- Separate location (into latitude and longitude) and format time. row is ($0, $1, $2, $3, $4)
step1 = FOREACH step0 GENERATE medallion, pickup_latitude, pickup_longitude, passenger_count, pickup_datetime;

--The above step changes timestamp to Unix format, alternatively, it is possible to have readable time strings

-- Select the points in US boundary (in this case, bounding box)
-- $2 is longitude, $1 is latitude. Times in epoch time: 1362895200  to 1363755600 
step2 = FILTER step1 BY $2 > -74.256090 AND $2 < -73.700273 AND $1 > 40.496111 AND $1 < 40.917585;
step3 = FILTER step2 BY $3 > 1;
step4 = FILTER step3 BY $4 >= '2013-03-10' AND $4 <= '2013-03-20';

-- Generate output
step5 = FOREACH step4 GENERATE $3;
STORE step5 INTO '${OUT_DIR}${input}_filtered.txt' USING PigStorage(',');

--dump COUNT(raw);
--dump COUNT(clean);
--dump COUNT(step0);
--dump COUNT(step1);
--dump COUNT(step2);
--dump COUNT(step3);
--dump COUNT(step4);
--dump COUNT(step5);
--STORE COUNT(step5) INTO 'step5_test.txt' USING PigStorage(',');
--a_all = GROUP step2 ALL;
--a_count = FOREACH a_all GENERATE COUNT(a);
--DUMP a_count;
