# Delete the previous data
clear
rm ny_taxi_march.csv_filtered.txt
hdfs dfs -rm -r ny_taxi_march.csv_filtered.txt
rm *.log
# Run the hadoop command

pig -f filter_ny.pig -param input=ny_taxi_march.csv

# Copy the output to local storage

hdfs dfs -getmerge ny_taxi_march.csv_filtered.txt ny_taxi_march.csv_filtered.txt


echo "Completed"
