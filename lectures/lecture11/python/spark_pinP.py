#!/usr/bin/env python

import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.storagelevel import StorageLevel
from pyspark import SparkFiles
conf = (SparkConf().setMaster("yarn-client").setAppName("point-in-polygon").set("spark.executor.memory", "4g").set("spark.executor.instances", 24))
sc = SparkContext(conf = conf)
#sc = SparkContext(appName="point-in-polygons")
sc.addPyFile("shapefile.py")
sc.addFile("quadTree.txt")
sc.addFile("us/us_states.shp")
sc.addFile("us/us_states.dbf")
sc.addFile("us/us_states.shx")

import shapefile
mShp = SparkFiles.get("us_states.shp")
mTree = SparkFiles.get("quadTree.txt")
e = shapefile.Editor(str(mShp))
e.buildQuadTree(str(mTree))

input = sc.textFile('hdfs://cg-hm08.ncsa.illinois.edu/user/jyn/day1_usa.txt').filter(lambda x:True).cache()

def getindex(line):
    tmp = line.split(',')
    #return tmp[-1]
    ind = e.index_of_first_feature_contains_point(float(tmp[2]),float(tmp[1]))
    if ind != -1:
        return line.strip() + ',' + str(ind)
    else:
        return line.strip() + ',' + '9999'

def f(line):
    tmp = line.strip().split(',')
    ind = tmp[-1]
    if ind != '9999':
        return line

#def main():
#    conf = (SparkConf().setMaster("yarn-client").setAppName("point-in-polygon").set("spark.executor.memory", "4g").set("spark.executor.instances", 50))
#    sc = SparkContext(conf = conf)
#    sc.addFile("shapefile.py")
#input = sc.textFile('week1_usa.txt').filter(lambda x:True).cache()

output = input.map(getindex).filter(f).saveAsTextFile('hdfs://cg-hm08.ncsa.illinois.edu/user/jyn/day1_usa_polygon.txt')

#input.saveAsTextFile('hdfs://cg-hm11.ncsa.illinois.edu/user/jyn/eee2.txt')
#output.filter(f).saveAsTextFile('hdfs://cg-hm11.ncsa.illinois.edu/user/jyn/eee.txt')

#output.saveAsTextFile('wsss3.txt')

#if __name__ == "__main__":
#        main()

