import sys
import time
import socket
from pyspark import SparkContext

# listening address
HOST = 'localhost'
PORT = 7748

def incoming(host, port):
    """Open specified port and return file-like object"""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((host, port))
    sock.listen(0)
    request, addr = sock.accept()
    return request.makefile('r', 0)

def makeIndex(record):
    '''filter and indexing target point records'''
    # Invalid Record Filter
    point_data = record.strip().split(",")
    if len(point_data) < 3:
        return []

    # Time Filter
    if point_data[5] == "":
        return []

    time = point_data[5]
    
    if time < start_time or time > end_time:
        return []

    # Lat Filter
    if point_data[11] == "":
        return []
    
    lat = float(point_data[11])
    
    if lat < start_lat or lat > end_lat:
        return []

    # Lon Filter
    if point_data[10] == "":
        return []
    
    lon = float(point_data[10])
    
    if lon < start_lon or lon > end_lon:
        return []

    # Empty Value Filter
    if point_data[7] == "":
        return []
    
    #radiation_value = float(point_data[3])

    passenger_count = int(point_data[7]);

    # Make Index
    #col_index = int( ( lon - start_lon ) / ( end_lon - start_lon ) * grid_width )
    #row_index = int( ( lat - start_lat ) / ( end_lat - start_lat ) * grid_height)
    
    row_index = int((lat - start_lat) / grid_width)
    col_index = int((lon - start_lon) / grid_height)

    #return [(format %(row_index, col_index),(radiation_value,1))]
    
    return [((row_index, col_index),(passenger_count, 1))]

if __name__ == "__main__":
    sc = SparkContext(appName="TaxiQuery")
    dataset = sc.textFile('hdfs://cg-hm11.ncsa.illinois.edu/user/jyn/trip_data_1.csv').filter(lambda x: True).cache()
    while True:
        print socket.gethostname() + '[]: Waiting for commands'
        for query in incoming(HOST, PORT):
	    print '[Processing]:' + query
            paras = query.split()
            start_time = paras[0]
            end_time = paras[1]
            start_lat = float(paras[2])
            end_lat = float(paras[3])
            start_lon = float(paras[4])
            end_lon = float(paras[5])
            grid_width = float(paras[6])    
            grid_width_mag = len(paras[6])
            grid_height = float(paras[7])
            grid_height_mag = len(paras[7])
            format="%0"+str(grid_height_mag)+"d:%0"+str(grid_width_mag)+"d"
            result = dataset.flatMap(makeIndex).reduceByKey(lambda a,b: (a[0]+b[0],a[1]+b[1])).map(lambda (key,value):(key,value[0]/value[1])).sortByKey()
            result.saveAsTextFile('hdfs://cg-hm11.ncsa.illinois.edu/user/jyn/radque_raster_%s.txt' %str(time.time()))
	    print '[Done]!!!!! '

    sc.stop()
