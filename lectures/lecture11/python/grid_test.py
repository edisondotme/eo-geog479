import numpy as np


def write_to_grid(result):
    nrows = int((end_lat - start_lat) / grid_width)+1
    ncols= int((end_lon - start_lon) / grid_height)+1
    xllcorner = start_lon
    yllcorner = start_lat

    cellsize = grid_width
    NODATA_value = -9999

    print ncols, nrows

    myArray = np.arange(ncols * nrows).reshape(nrows,ncols)

    myArray.dtype =  np.float64

    for i in range(nrows):
        for j in range(ncols):
            myArray[i,j]= NODATA_value

    #((90, 130), 1.0)
    for ele in result:
        myArray[ele[0][0], ele[0][1]] = float(ele[1])


    header = "ncols     %s\n" % myArray.shape[1]
    header += "nrows    %s\n" % myArray.shape[0]
    header += "xllcorner %s\n" % xllcorner
    header += "yllcorner %s\n" % yllcorner
    header += "cellsize %s\n" % cellsize
    header += "NODATA_value -9999\n"

    f = open("myGrid.asc", "w")
    f.write(header)
    np.savetxt(f, myArray, fmt="%1.2f")
    f.close()


if __name__ == '__main__':
    write_to_grid()

