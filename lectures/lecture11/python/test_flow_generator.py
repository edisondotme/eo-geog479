import numpy as np
from pyspark import SparkConf, SparkContext
import shapefile
import sys
import os

e=shapefile.Editor('us/_states.shp')
e.buildQuadTree('quadTree.txt')

def pt_index(record):
	#pt[0]: user_id
	#pt[1]: latitude
	#pt[2]: longitude
	#pt[3]: timestamp
	pt = record.strip().split(",")
	ind = e.index_of_first_feature_contains_point(float(pt[2]),float(pt[1]))
    	if ind !=-1:
        	#xx = e.records[int(ind)][1]
        	return line.strip() + ',' + str(ind)
        	#return line.strip()+','+ str(ind) + ',' + str(xx)
def flow_index(record):
	row = record.strip().split(",")
	return [(row[0], (row[-2], row[-1]))]


def main():
	conf = (SparkConf().setMaster("yarn-client").setAppName("flow_generator").set("spark.executor.memory", "4g"))
	sc = SparkContext(conf = conf)
	#sc = SparkContext(appName="example")
	#print 'hello world'
	data = sc.textFile("hdfs://cg-hm11.ncsa.illinois.edu/user/jyn/test2.txt")
	data.flatMap(flow_index)
	#.reduceByKey(lambda a,b: (a[0]+b[0],a[1]+b[1])).map(lambda (key,value):(key,value[0]/value[1])).sortByKey()

if __name__ == "__main__":
	main()
