import pyspark
#from pyspark.context import SparkContext
from pyspark import SparkConf, SparkContext
from pyspark.storagelevel import StorageLevel

def getindex(line):
    tmp=line.strip().split(',')
    return (tmp[0],(tmp[-2],tmp[-1]))

def gentraj(ulpair):
    tmp = sorted(ulpair[1])
    ans = []
    previous = tmp[0][1]
    for i in range(1,len(tmp)):
        current = tmp[i][1]
        if current != previous:
            ans.append(((previous,current),1))
            previous = current
    return ans

def export_to_d3(result):
    names = ["Arizona", "Arkansas", "Colorado", "Connecticut", "District of Columbia", "Georgia", "Idaho", "Illinois", "Iowa", "Kansas", "Louisiana", "Maryland", "Minnesota", "Missouri", "Montana", "Nevada", "New Jersey", "North Dakota", "Ohio", "Oklahoma", "Pennsylvania", "South Carolina", "South Dakota", "Utah", "Vermont", "West Virginia", "Wyoming", "New Hampshire", "Tennessee", "Florida", "Massachusetts", "Virginia", "Alabama", "California", "Delaware", "Indiana", "Kentucky", "Maine", "Michigan", "Mississippi", "Nebraska", "New Mexico", "New York", "Oregon", "Rhode Island", "Texas", "Washington", "Wisconsin", "North Carolina"]
    ids = [x for x in xrange(49)]
	
    mOutFile = open("/home/jyn/spark/d3/matrix.json","wb")
    mTable = [[0] * 49 for x in xrange(49)]
    mWeight = [0 for x in xrange(49)]

    for row in result:
        #((u'15', u'41'), 105)
        source = row[0][0]
        target = row[0][1]
        weight = row[1]
        mTable[int(target)][int(source)] = int(weight)
    mOutFile.write('[')
    for i in range(49):
        mOutFile.write(str(mTable[i]))
        mOutFile.write(',\n')
    mOutFile.write(']')
    mOutFile.close()


def main():
    conf = (SparkConf().setMaster("yarn-client").setAppName("flow_generator").set("spark.executor.memory", "4g").set("spark.executor.instances", 50))
    sc = SparkContext(conf = conf)
    #sc = SparkContext(appName = "flow_generator")
    #input = sc.textFile('hdfs://cg-hm03.ncsa.illinois.edu/user/jyn/test.txt') 
    input = sc.textFile('usa_polygon_feb.txt').filter(lambda x:True).cache()
    #input = sc.textFile("test3.txt")
    output = input.map(getindex).aggregateByKey(set(), lambda s, d: s.add(d) or s, lambda s1, s2: s1.union(s2)).flatMap(gentraj).reduceByKey(lambda a,b: a + b)
    
    ##output.saveAsTextFile('hdfs://cg-hm11.ncsa.illinois.edu/user/jyn/sss.txt')
    
    #output.saveAsTextFile('us_twitter_feb_flow.txt')
    
    result = output.collect()
    export_to_d3(result)
    print 'Done'

if __name__ == "__main__":
        main()
