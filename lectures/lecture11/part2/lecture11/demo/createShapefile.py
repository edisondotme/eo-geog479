import shapefile

w = shapefile.Writer(shapefile.POLYLINE)

mFile = open("chicago_traj_sample.txt","rb")

w.field('user_id','C','40')

for mLine in mFile:
    #pt = []
    line = mLine.split(',')
    mNumber = len(line)
    if mNumber > 3:
        ptm = []
        userid = line[0]
        for mPoints in line[1:]:
            [longitude,latitude,mTime, polyID,attribute4] = mPoints.split("&")
            ptm.append([float(longitude),float(latitude)])
        w.line(parts=[ptm])
        #w.poly(parts=[pt], shapeType=shapefile.POLYLINE)
        w.record(str(userid))

#w.field('SECOND_FLD','C','40')

w.save('shapefiles/chicago')
print 'finished'

if __name__ == "__main__":
    print "running"
