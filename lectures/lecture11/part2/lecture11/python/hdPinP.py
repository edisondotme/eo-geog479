#!/usr/bin/env python
import shapefile
import sys
import os

e=shapefile.Editor('app_top_locations/chicago/chicago_landuse_wgs84.shp')
e.buildQuadTree('app_top_locations/quadTree.txt')

for line in sys.stdin:
    tmp = line.strip().split(',')
    ##ind=e.index_of_first_feature_contains_point(float(tmp[2]),float(tmp[1]))
    ind=e.index_of_nn_feature(float(tmp[2]),float(tmp[1]))
    if ind !=-1:
        xx = e.records[int(ind)][1]
        print line.strip()+','+ str(ind) + ',' + str(xx)

##    [mID, mDist]=e.dist_to_boundary(float(tmp[1]),float(tmp[2]))
##    if mID != -1:
##        print line.strip()+','+str(mID)+','+str(mDist)

