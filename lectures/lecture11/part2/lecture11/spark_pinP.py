#!/usr/bin/env python

import pyspark
from pyspark import SparkConf, SparkContext
from pyspark.storagelevel import StorageLevel
from pyspark import SparkFiles


#conf = (SparkConf().setMaster("yarn-client").setAppName("point-in-polygon").set("spark.executor.memory", "2g").set("spark.executor.instances", 16))
#sc = SparkContext(conf = conf)
sc = SparkContext(appName="tweets in states")
sc.addFile("python/shapefile.py")
sc.addFile("python/quadTree.txt")
sc.addFile("python/us/us_states.shp")
sc.addFile("python/us/us_states.dbf")
sc.addFile("python/us/us_states.shx")

import shapefile
mShp = SparkFiles.get("us_states.shp")
mTree = SparkFiles.get("quadTree.txt")
e = shapefile.Editor(str(mShp))
e.buildQuadTree(str(mTree))

input = sc.textFile('hdfs://cg-hm08.ncsa.illinois.edu/user/orellan2')
##.filter(lambda x:True).cache()

def getindex(line):
    tmp = line.split(',')
    ind = e.index_of_first_feature_contains_point(float(tmp[2]),float(tmp[1]))
    if ind != -1:
        return (ind, tmp[ind]) 
    else:
        pass


output = input.map(getindex).reduceByKey(lambda a,b: a+b).sortByKey()
output.saveAsTextFile('hdfs://cg-hm08.ncsa.illinois.edu/user/orellan2')
