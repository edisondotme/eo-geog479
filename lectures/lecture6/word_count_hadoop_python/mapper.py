#!/usr/bin/env python

import sys


for line in sys.stdin:
    line = line.strip().replace('(','').replace(')','').replace(':','').replace('.','').replace(';','')
    words = line.replace(',','').split()

    for word in words:
        print '%s,%s' % (word, 1)
