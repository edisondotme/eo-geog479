#!/usr/bin/env python

import sys
import os

#def main():
#    for line in sys.stdin:
#        pass

def makeIndex(record):
    # Invalid Record Filter
    point_data = record.strip().split(",")
    if len(point_data) < 3:
        return []

    # Time Filter
    if point_data[5] == "":
        return []

    time = point_data[5]
    if time < start_time or time > end_time:
        return []
    # Lat Filter
    if point_data[11] == "":
        return []
    
    lat = float(point_data[11])
    if lat < start_lat or lat > end_lat:
        return []
    
    # Lon Filter
    if point_data[10] == "":
        return []
    
    lon = float(point_data[10])
    if lon < start_lon or lon > end_lon:
        return []
    
    # Empty Value Filter
    if point_data[7] == "":
        return []
    
    passenger_count = int(point_data[7])
    
    # Make Index
    #col_index = int( ( lon - start_lon ) / ( end_lon - start_lon ) * grid_width )
    #row_index = int( ( lat - start_lat ) / ( end_lat - start_lat ) * grid_height)
    
    row_index = int((lat - start_lat) / grid_width)
    col_index = int((lon - start_lon) / grid_height)
    #print [(format %(row_index, col_index),(radiation_value,1))]
    return [(row_index, col_index),(passenger_count, 1)]


paras = sys.argv[-9:]
start_time = paras[0]
end_time = paras[1]
start_lat = float(paras[2])
end_lat = float(paras[3])
start_lon = float(paras[4])
end_lon = float(paras[5])
grid_width = float(paras[6])
grid_width_mag = len(paras[6])
grid_height = float(paras[7])
grid_height_mag = len(paras[7])
    
for line in sys.stdin:
    ww=makeIndex(line)
    if ww != []:
        print ww
