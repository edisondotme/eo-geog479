##2013-01-01 2013-02-01 40.479636 40.930724 -74.402322 -73.630027 0.005 0.005
echo Start time: $1 Stop time: $2 
echo "lower left (latitude, longitude):" $3, $5
echo "upper right (latitude, longitude):" $4, $6
echo width,height: $7, $8
echo output file: $9


last_arg="${!#}"

##array=${@:1:$#-1}
array=${@}
echo "program starts"

yarn jar /usr/hdp/2.3.2.0-2602/hadoop-mapreduce/hadoop-streaming-2.7.1.2.3.2.0-2602.jar -mapper "mapper.py ${array}" -file mapper.py -reducer "reducer.py" -file reducer.py -input trip_data_1.csv -output result.txt -numReduceTasks 10

hdfs dfs -getmerge result.txt xx.txt
#export PATH=/sw/anaconda/bin:$PATH
#export PYTHONPATH=/sw/anaconda/lib/python2.7/site-packages:$PYTHONPATH
module load anaconda
python write2Grid.py ${array}
echo "deleting intermediate results"
hdfs dfs -rm -r result.txt
rm xx.txt
