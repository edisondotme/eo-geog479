
# coding: utf-8

# ##Import the necessary modules

# In[2]:

#!/usr/bin/env python

import fileinput
import string

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from scipy.misc import factorial

#Pandas depends on matplotlib - set up a temp dir for config 
import os
import tempfile
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
from pandas import DataFrame


# ## Define the run_kmeans function

# In[3]:

def run_kmeans(dataset, max_iterations=100, num_clusters=5, num_seeds=5):
#    vectorizer = TfidfVectorizer(stop_words='english', use_idf=True)
#    feature_vectors = vectorizer.fit_transform(dataset["content"])
    geo_locations = DataFrame(dataset,columns=['lat','lon'])
    geo_locations = DataFrame.as_matrix(geo_locations, columns=None) 
    #TODO: Create a KMeans object
    km = KMeans(n_clusters=num_clusters, init='k-means++',
    max_iter=max_iterations, n_init=num_seeds, verbose=0)
    #TODO: Compute cluster centers and predict cluster index for each newsgroup topic
    clusters = km.fit_predict(geo_locations)
    #TODO: Return the results in a new DataFrame
    result = DataFrame(dataset, columns = ["id","time","request","lat","lon"])
    result["cluster_id"] = clusters
    return result


# ##Process the input data (line by line)

# In[4]:

dataset = []
for line in fileinput.input():
    line = line.strip()
    fields = line.split('\t')    
    dataset.append({"id": fields[0], "time": fields[1], "request": fields[2],
                    "lat": fields[3], "lon": fields[4]})
       


# ##Pass the data to the clustering function

# In[4]:

#print (dataset)
clusters = run_kmeans(DataFrame(dataset))
#print(DataFrame(clusters,index = False))
print(DataFrame.to_string(clusters, header = False, index = False))
#print(DataFrame.to_string(dataset, header = False, index = False)) 


# In[ ]:



