--use the DEFINE command to assign an alias to a streaming command
define kmeans311  `/sw/anaconda/bin/python kmeans311.py` ship ('kmeans311.py');

--remove a file in HDFS if the same name file exists.
rmf kmeans_output;

--load data.
raw = LOAD '311_data.csv' USING PigStorage(',') AS (id:chararray,time:chararray,request:chararray,Lat:double,Lon:double); 

--filter out the data from step0 to step4
step0 = FILTER raw BY time is not null AND time != 'null' AND time != '';

step1 = FILTER step0 BY $0 is not null;

step2 = FILTER step1 BY $1 is not null;

step3 = FILTER step2 BY $2 is not null and $2 MATCHES 'Graffiti Removal';
--'Graffiti Removal';
-- 'Tree Debris';
-- 'Pothole in Street';
-- 'Abandoned Vehicle Complaint';
--Alley Light Out';

step4 = FILTER step3 BY ($1 > '2014-07-15 00:00:00' AND $1 < '2014-08-01 00:00:00');

--generate new records to send down the pipeline to the next operator.
step5 = FOREACH step4 GENERATE $0,$1,$2,$3,$4;

--streams the data through the kmeans311.py script.
result = STREAM step5 through kmeans311;

--dump result;

--store the results into a folder in HDFS.
store result into 'kmeans_output';
