
# coding: utf-8

# In[56]:

if __name__ != '__lib__':
    def outputSchema(empty):
        return lambda x: x


# In[64]:

def getOutlierRanges(myList):
    myList.sort()
    #length 
    temp_len = len(myList)

    #Even or Odd number
    var1 = temp_len % 2
    #middle point
    midPt = temp_len / 2
    median = 0.0  #median
    Q1 = 0.0  #lower quartile
    Q3 = 0.0  #upper quartile
    if var1 == 0:
        #print "1 - Even number"
        median = (myList[midPt - 1] + myList[midPt])/2.0
        Q1MidPt = midPt / 2
        Q1 = (myList[Q1MidPt - 1] + myList[Q1MidPt])/2.0
        Q3MidPt = (temp_len + midPt) / 2
        Q3 = (myList[Q3MidPt - 1] + myList[Q3MidPt])/2.0    
    else:
        #print "1 - Odd number"
        median = myList[midPt]
        Q1MidPt = midPt / 2
        Q1 = myList[Q1MidPt]
        Q3MidPt = (temp_len + midPt) / 2
        Q3 = myList[Q3MidPt]
        
    #interquartile range 
    interQRange = Q3 - Q1

    #minor outliers
    innerFences = Q1 - 1.5*interQRange
    outerFences = Q3 + 1.5*interQRange

    #major outliers
    #innerFences = Q1 - 3*interQRange
    #outerFences = Q3 + 3*interQRange
  
    return innerFences, outerFences


# In[70]:

@outputSchema("fromAverage:double")
def getAverage(cpm):
    cpmList = []
    #extract cpm values
    for record in cpm:
        cpmList.append(record[0])
        
    #calculate inner fences and outer fences
    inner_fences, outer_fences = getOutlierRanges(cpmList)
    
    inputSum = 0.0
    i = 0.0
    tmpSum = 0.0
    j = 0.0
    #sum up cpm
    for record in cpm:
        if record[0] > inner_fences and record[0] < outer_fences:
            inputSum += record[0]
            i = i + 1
        else:
            tmpSum += record[0]
            j = j + 1
    if i == 0:
        return tmpSum/j
    else:
        return inputSum/i
   


# In[69]:

# In[ ]:




# In[ ]:



