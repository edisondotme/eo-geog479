--remove a file in HDFS if the same name file exists.
rmf heatmap_output;

--load data.
raw = LOAD 'radiation.csv' USING PigStorage(',') AS (CapturedTime : chararray,Latitude:double,Longitude:double,Value:int,Unit:chararray);

--filter out the data based on the bounding box of Koriyama, Japan.
step1 = FILTER raw BY $2 > 140.32564 AND $2 < 140.40993 AND $1 > 37.36191 AND $1 < 37.43678;

--select the rows of which CapturedTime is August.
step2 = FILTER step1 BY ($0 > '2013-08-01 00:00:00' AND $0 < '2013-09-01 00:00:00');

--generate new records to send down the pipeline to the next operator.
step3 = FOREACH step2 GENERATE $0,$1,$2,$3,$4;

--generate new records based on latitude and longitude  key.
step4 = FOREACH step3 GENERATE $3,(int)(($2 - 140.32564)/0.005) AS logkey,(int)(($1 - 37.36191)/0.005) AS latkey;

--collect all records with the same latitude and longitude key.
step4_group = GROUP step4 BY (latkey,logkey);


--calculate the average of radiation levels per each cell.
step4_avg = FOREACH step4_group GENERATE group, AVG(step4.Value);

--sort data based on the group.
result = ORDER step4_avg BY group;

--dump the results to your screen
--dump result;

--send the output to a folder in HDFS
store result into 'heatmap_output';
