
# coding: utf-8

# ##Import the Module

# In[1]:

import numpy as np


# ## Define a grid header

# In[2]:

ncols = 17            # a number of columns in the grid
nrows = 15            # a number of rows
xllcorner = 140.32564 # the x coordinate of the lower left corner, which is the minimum x value
yllcorner = 37.36191  # the y coordinate of the lower left corner, which is the minimum y value
cellsize = 0.005      # resolution of the raster
NODATA_value = -9999  # no data value


# ## Reading grids

# In[3]:

myArray = np.arange(255).reshape(nrows,ncols)
myArray.dtype =  np.float64 

#Initialize myArray
for i in range(nrows) :
    for j in range(ncols):
        myArray[i,j]= NODATA_value     


#Reading a grid file
with open('heatmap.txt') as my_file:
    for line in my_file:
        #print line
        tokens=line.strip().replace(')', ',').replace('(', '').replace('\t', '').split(',')
        #print tokens
        myArray[(nrows-1) - int(tokens[0]), int(tokens[1])]= float(tokens[2])
        
#print myArray        
        


# ## Writing an ASCIIGRID file

# In[4]:

header = "ncols     %s\n" % myArray.shape[1]
header += "nrows    %s\n" % myArray.shape[0]
header += "xllcorner %s\n" % xllcorner
header += "yllcorner %s\n" % yllcorner
header += "cellsize %s\n" % cellsize
header += "NODATA_value %s\n" % NODATA_value

f = open("heatmap_image.asc", "w")
f.write(header)
np.savetxt(f, myArray, fmt="%1.2f")
f.close()


# In[ ]:



