#!/usr/bin/env python

import sys

def main():
    #set the current key to None
    current_word = None
    #current_key = None
    
    #set the current count value to 0
    current_count = 0
    #current_value = None

    word = None

    #the incoming data are the key,value pairs from mapper, e.g. word_a,1
    for line in sys.stdin:
        l = line.split(',')
        word = l[0]
        word_count = l[1]

        if current_word == None:
            current_word = word

        if current_word == word:
            current_count += int(word_count)
        else:
            result = (current_word, current_count)
            print(",".join(str(v) for v in result))
            current_word = word
            current_count = int(word_count)
    if current_word == word:
        result = (current_word, current_count)
        print(",".join(str(v) for v in result))


if __name__=="__main__":
    main()
