import sys
import time
from pyspark import SparkContext
import math
import numpy as np

def makeIndex(record):
    '''filter and indexing target point records'''
    # Invalid Record Filter
    point_data = record.strip().split(",")
    if len(point_data) < 3:
        return []

    # Time Filter
    if point_data[5] == "":
        return []

    time = point_data[5]

    if time < start_time or time > end_time:
        return []

    # Lat Filter
    if point_data[11] == "":
        return []

    lat = float(point_data[11])

    if lat < start_lat or lat > end_lat:
        return []

    # Lon Filter
    if point_data[10] == "":
        return []

    lon = float(point_data[10])

    if lon < start_lon or lon > end_lon:
        return []

    # Empty Value Filter
    if point_data[7] == "":
        return []

    passenger_count = int(point_data[7]);

    # This needs to be fixed
    row_index = int((end_lat - start_lat) / grid_size) + 1 
    col_index = int((end_lon - start_lon) / grid_size) + 1 

    return [((row_index, col_index),(passenger_count, 1))]

def write_to_grid(result):
    nrows = int((end_lat - start_lat) / grid_size)+1
    ncols= int((end_lon - start_lon) / grid_size)+1
    xllcorner = start_lon
    yllcorner = start_lat
    cellsize = grid_size
    NODATA_value = -9999

    #print ncols, nrows

    myArray = np.arange(ncols * nrows).reshape(nrows,ncols)

    myArray.dtype =  np.float64

    for i in range(nrows):
        for j in range(ncols):
            myArray[i,j]= NODATA_value

    #((90, 130), 1.0)
    for ele in result:
        myArray[ele[0][0], ele[0][1]] = float(ele[1]) 

    header = "ncols     %s\n" % myArray.shape[1]
    header += "nrows    %s\n" % myArray.shape[0]
    header += "xllcorner %s\n" % xllcorner
    header += "yllcorner %s\n" % yllcorner
    header += "cellsize %s\n" % cellsize
    header += "NODATA_value -9999\n"

    f = open("myGrid.asc", "w")
    f.write(header)
    np.savetxt(f, myArray, fmt="%1.2f")
    f.close()



if __name__ == "__main__":
    sc = SparkContext(appName="TaxiDesityMap")
    # Change the file path to something that actually exists
    dataset = sc.textFile('hdfs://cg-hm08.ncsa.illinois.edu/user/orellan2/eo-geog479/labs/lab10/trip_data_1.csv').filter(lambda x: True).cache()
    paras = sys.argv[-7:]

    start_time = paras[0]
    end_time = paras[1]
    start_lat = float(paras[2])
    end_lat = float(paras[3])
    start_lon = float(paras[4])
    end_lon = float(paras[5])
    grid_size = float(paras[6])

    result = dataset.flatMap(makeIndex).reduceByKey(lambda a,b: (a[0]+b[0],a[1]+b[1])).map(lambda (key,value):(key,value[0]/(value[1]*1.0))).sortByKey()
    ww = result.collect()
    ##print max([l[0][0] for l in ww]), max([k[0][1] for k in ww])
    write_to_grid(ww)
    
    # change the file path to something that acutally exists
    result.saveAsTextFile('hdfs://cg-hm08.ncsa.illinois.edu/user/orellan2/eo-geog479/labs/lab10/radque_raster_%s.txt' %str(time.time()))
    print '[Done]!!!!!'
