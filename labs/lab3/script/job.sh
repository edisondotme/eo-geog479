#!/bin/bash
### Example job submission on Roger (roger-login.ncsa.illinois.edu)
### Aiman Soliman (asoliman@illinois.edu)

### Set the job name
#PBS -N job_pitremoval_junjun

### Use the bourne shell
#PBS -S /bin/bash

### To send email when the job is completed:
### be --- before execution
### ae --- after execution
#PBS -m ae
#PBS -M jyn@illinois.edu

### Optionally set the destination for your program's output
### Specify localhost and an NFS filesystem to prevent file copy errors.
### #PBS -e localhost:$HOME/myjob.err
### #PBS -o localhost:$HOME/myjob.log

### Specify the number of cpus for your job. 
#PBS -l nodes=2:ppn=20
###printenv

### Tell PBS how much memory you expect to use. Use units of 'b','kb', 'mb' or 'gb'.
###PBS -l mem=8gb

### Tell PBS the anticipated run-time for your job, where walltime=HH:MM:SS
#PBS -l walltime=1:00:00


module load mpich taudem
echo "Getting pit removed DEM"
mkdir ~/lab3/output
mpirun -n $PBS_NP pitremove -z /home/cgtrn50/lab3/data/u39w120.tif -fel $HOME/lab3/output/


# End of PBS script
