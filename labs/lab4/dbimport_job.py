from pymongo import MongoClient
import datetime
import json

def main():
	client = MongoClient('mongodb://141.142.168.54:27017')
	db = client.jyn_job
	collection = db['chicago']

	mFile = open("/gpfs_scratch/geog479/lab4/2014_07_04_chi.txt","rb")
	for mLine in mFile:
		# print mLine
		[uid, lat, lng, tm, msg] = mLine.split("\t")
		latt = float(lat)
		lngg = float(lng)
		geoString =  {'user_id': uid, 'time': tm, 'message': msg, 'loc': {'type' : 'Point', 'coordinates' : [lngg, latt]}}
		print geoString               
		db.chicago.insert(geoString)

if __name__ == '__main__':
	main()
