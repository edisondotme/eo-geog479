-- Load the Data
raw = LOAD 'Twitter_US_day1.txt' USING PigStorage('\u0001') AS (tweetId,text:chararray,geo,source,isretweet,search_id,create_at,meta_data,to_user_id,language_code, user_id,profile_image_url,user_name,place_id,place_name,place_fullname,country,place_type,street_address,boundary,boundary_type,place_url);

-- Filter out duplicates
clean = DISTINCT raw;

--Filter out missing data
step0 = FILTER clean BY geo is not null AND geo != 'null' AND  geo != '' AND isretweet == 'false' AND create_at is not null AND create_at != 'null' AND create_at != '';

-- Separate location (into latitude and longitude) and format time
--step1 = FOREACH step0 GENERATE user_id, flatten(STRSPLIT(geo, ',')), REPLACE(REPLACE(create_at, 'CDT', '-05:00'), 'CST', '-06:00'),text;

--The above step changes timestamp to Unix format, alternatively, it is possible to have readable time strings
step1 = FOREACH step0 GENERATE user_id, flatten(STRSPLIT(geo, ',')), create_at, text;

-- Select the points in US boundary (in this case, bounding box)
-- $2 is longitude, $1 is latitude
-- New York
--step2 = FILTER step1 BY $2 > -74.256090 AND $2 < -73.700273 AND $1 > 40.496111 AND $1 < 40.917585;

--Chicago
step2 = FILTER step1 BY $2 > -88.707599 AND $2 < -87.524535 AND $1 > 41.201577 AND $1 < 42.495775;

-- Generate output
step3 = FOREACH step2 GENERATE $0, $1, $2, $3, $4;
STORE step3 INTO 'result.txt' USING PigStorage(',');

-- Filter keywords and generate the output for scenario table
step4 = FILTER step3 BY $4 MATCHES '.*(happy | sad).*';
step5 = FOREACH step4 GENERATE $0,$1,$2,$3,$4;
STORE step5 INTO 'result_keywords.txt' USING PigStorage(',');
