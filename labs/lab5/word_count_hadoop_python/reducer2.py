#!/usr/bin/env python

import sys

def main():
    word2count = {}
    for line in sys.stdin:
        l = line.strip().split(',')
        word = l[0]
        word_count = int(l[1])
        try:
            word2count[word] = word2count[word] + word_count
        except:
            word2count[word] = word_count
    for key in word2count.keys():
        print '%s,%s'% (key, word2count[key] )


if __name__=="__main__":
    main()
