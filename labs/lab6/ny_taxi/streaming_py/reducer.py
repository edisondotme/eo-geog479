#!/usr/bin/env python

import sys
from ast import literal_eval
#(32, 122):(1, 1)
def main():
    curKey = None
    curPassenger = 0
    curTaxi = 0

    for line in sys.stdin:
        l = literal_eval(line)
        key = l[0]
        passengerCount = l[1][0]
        taxiCount = l[1][1]
        if curKey == None:
            curKey = key
        
        if curKey == key:
            curPassenger += passengerCount
            curTaxi += 1
        else:
            result = [key, curPassenger/(curTaxi*1.0)]
            print(",".join(str(v) for v in result))
            curKey = key
            curPassenger = passengerCount
            curTaxi = taxiCount
    
    print(",".join(str(v) for v in result))

if __name__ == "__main__":
    main()

