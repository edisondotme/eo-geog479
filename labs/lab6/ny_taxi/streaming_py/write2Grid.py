import sys
import numpy as np
from ast import literal_eval

def write_to_grid(path):
    
    nrows = int((end_lat - start_lat) / grid_width)+1
    ncols= int((end_lon - start_lon) / grid_height)+1
    xllcorner = start_lon
    yllcorner = start_lat

    cellsize = grid_width
    NODATA_value = -9999

    #print ncols, nrows

    myArray = np.arange(ncols * nrows).reshape(nrows,ncols)

    myArray.dtype =  np.float64

    for i in range(nrows):
        for j in range(ncols):
            myArray[i,j]= NODATA_value

    #((90, 130), 1.0)
    #(0, 108),1.0
    result = open("xx.txt", 'rb')
    for line in result:
        ele = literal_eval(line)
        myArray[ele[0][0], ele[0][1]] = float(ele[1])


    header = "ncols     %s\n" % myArray.shape[1]
    header += "nrows    %s\n" % myArray.shape[0]
    header += "xllcorner %s\n" % xllcorner
    header += "yllcorner %s\n" % yllcorner
    header += "cellsize %s\n" % cellsize
    header += "NODATA_value -9999\n"

    f = open(path, "w")
    f.write(header)
    np.savetxt(f, myArray, fmt="%1.2f")
    f.close()
#np.savetxt("myGrid.asc", myArray, header=header, fmt="%1.2f")

if __name__=='__main__':
    paras = sys.argv[-9:]
    start_time = paras[0]
    end_time = paras[1]
    start_lat = float(paras[2])
    end_lat = float(paras[3])
    start_lon = float(paras[4])
    end_lon = float(paras[5])
    grid_width = float(paras[6])
    grid_width_mag = len(paras[6])
    grid_height = float(paras[7])
    grid_height_mag = len(paras[7])
    path = paras[8]
    write_to_grid(path)
